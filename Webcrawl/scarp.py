# Import libraries
import requests
from bs4 import BeautifulSoup


#collect first page
page = requests.get('https://web.archive.org/web/20121007172955/https://www.nga.gov/collection/anZ1.htm')

#BS4 object
soup = BeautifulSoup(page.text, 'html.parser')

# Remove bottom links
last_links = soup.find(class_='AlphaNav')
last_links.decompose()


# all text from class BodyText 
ar_list = soup.find(class_= 'BodyText')

#print(ar_list)
a_tags = ar_list.find_all('a')

for ar in a_tags:
  names = ar.contents[0]
  print(names)
  
  
  