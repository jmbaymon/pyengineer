#try except is for  contining code  
# astr = 'Hello Bob'

# try:
#   istr = int(astr)
  
# except: istr = -1
  
# print('First', istr)

# astr = '1234'

# try:
#   istr = int(astr)
  
# except: istr = -1
  
# print('First', istr)

'''
3.1 Write a program to prompt the user for hours and rate per hour using input to compute gross pay. Pay the hourly rate for the hours up to 40 and 1.5 times the hourly rate for all hours worked above 40 hours. Use 45 hours and a rate of 10.50 per hour to test the program (the pay should be 498.75). You should use input to read a string and float() to convert the string to a number. Do not worry about error checking the user input - assume the user types numbers properly. '''

hrs = input("Enter Hours:")

rate = input("Enter Rate:")

h = float(hrs)
r = float(rate)

if h > 40:
  # is total amount of hours done
  overtime = 1.5 * r * (h - 40) 
  
  # 40  is the  standard max for the regular rate 
  gross_pay = 40 * r 
  gross_pay = gross_pay + overtime
  
else:
   gross_pay = h * r

print(gross_pay)


