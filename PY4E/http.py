import socket

mysock= socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#make connection to port 80
mysock.connect(('data.pr4e.org', 80)) 
#send blank line 
cmd = 'GET http://data.pr4e.org/romeo.txt HTTP/1.0\r\n\r\n'.encode() 
mysock.send(cmd)

while True:
  #reiecve data 
  data = mysock.recv(20)
  if (len(data) < 1):
      break
  print(data.decode(), end='')
  
mysock.close()