from exper import exp

class Menu():
    def __init__(self):
        pass
    
    def menu_display(self):
        option = None
        opt_control = True

        while opt_control:    
            print("Main Menu")
            print("1.Add Entry")
            print("2.Display all Entries")
            print("3.Remove Entry:Coming Soon")
            print("To quit enter q!")
            option = input("Select an Option: ")
    
            if option == "1":
               self.add_entry()
            elif option == "2":
               self.display_entries()
            elif option == "q":
                opt_control = False 
            return option   
      
    def add_entry(self):
        temp = exp()
        description = input("Enter Description: ")
        cost = input("Enter Cost: $")
        return temp.add(description, cost)
    
    def display_entries(self):
        temp = exp()
        temp.display()
        return 0
              