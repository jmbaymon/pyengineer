import sqlite3
class exp:
    def __init__(self):
        pass

    #add description and cost. also used menu
    def add(self, desc, cost):
        db_desc = desc
        db_cost = cost
        return self.db_ops(db_desc,db_cost)           
    
    #display db table     
    def display(self):
        #open connection 
        conn = sqlite3.connect('cost.db')
        c = conn.cursor() 
        #perform select all query and prints out table.  
        c.execute("SELECT * FROM Costs") 
        #Print out Table 
        for row in c.fetchall():
            print(row)
        #closes connection
        conn.commit()
        c.close()
        conn.close()


    '''This function does the following:
    1. Create a database name cost.db
    2. Create a table name Costs
    3. Insert two values for desc and cost
    4.Finally close connection to the  database 
    '''
    
    def db_ops(self,db_desc, db_cost):
        #open connection 
        conn = sqlite3.connect('cost.db')
        c = conn.cursor() 
        #create table
        c.execute("create table if not exists Costs (desc TEXT ,cost Real)")
        #insert values in database 
        c.execute("insert into Costs(desc, cost) values(?,?)" ,(db_desc , db_cost ))
        #closes connection
        conn.commit()
        c.close()
        conn.close()

        